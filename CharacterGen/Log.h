#ifndef LOG_H

#define LOG_H

class Log {
public:
	static void Msg(std::string msg);
	static void Error(std::string msg);
	static void Debug(std::string msg);
};
#endif // !LOG_H
