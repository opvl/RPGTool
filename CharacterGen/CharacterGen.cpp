#include "stdafx.h"

/* const internal values */
constexpr float pr_version = 0.2f;
constexpr char pr_name[] = "OPVL's Character Generation Tool";

bool core() {
	while (running) {
		/* core loop */

		currentPlayers[1].action->attack(currentPlayers[0]);

		//Exit Command
		running = !GetAsyncKeyState(VK_ESCAPE) & 1;
	}

	//correct return value
	return true;
}

Ini names("names", "opal\\resources\\");
Ini config("config");

void loadNames() {
	
	Log::Msg("Loading Names. . .");
	numName = names.numKeys("Names") - 1;
	numPrefixes = names.numKeys("Prefixes") - 1;
	numTitles = names.numKeys("Titles") - 1;

	for (int i = 0; i <= numName; i++) {
		ostringstream oss;
		oss << i;
		n_names.push_back(names.read("Names", oss.str()));
		Log::Debug(n_names[i]);
	}
	ostringstream ss;
	ss << n_names.size();
	Log::Msg(ss.str() + " Names Loaded.\n\nLoading Prefixes. . .");

	for (int i = 0; i <= numPrefixes; i++) {
		ostringstream iss;
		iss << i;
		n_prefix.push_back(names.read("Prefixes", iss.str()));
		Log::Debug(n_prefix[i]);
	}
	ostringstream as;
	as << n_prefix.size();
	Log::Msg(as.str() + " Prefixes Loaded.\n\nLoading Titles. . .");

	for (int i = 0; i <= numTitles; i++) {
		ostringstream ass;
		ass << i;
		n_title.push_back(names.read("Titles", ass.str()));
		Log::Debug(n_title[i]);
	}
	ostringstream os;
	os << n_title.size();
	Log::Msg(os.str() + " Titles Loaded.");
}

int main() {

	debug = config.read_b("Debug", "Logging");

	Log::Msg("Loading Resources. . .");
	loadNames();
	Log::Msg("Done.\n");

	/* Welcome Mesaage and version string */
	cout << pr_name << " : version " << pr_version << endl << flush;

	cout << endl << "Loading Player. . ." << endl << flush;

	cout << "Enter Name: " << flush;
	string pName;
	cin >> pName;

	Character player_1(pName);
	currentPlayers.push_back(player_1);
	Character player_2("Steve");
	currentPlayers.push_back(player_2);

	running = true;

	core();

	system("pause");
	return 0;
}
