#include "stdafx.h"

/* program internal values */
bool running = false;
bool debug = true;
int in_id = 0;

/* database lookup values */
bool db_searching = false; //to initiate loop as db is queried
bool db_idfound = false; //subloop to find userID
char* db_rName = "*invalid*"; //return name if ID is found
int db_rAge = 0; //return age if ID is found (level)

vector<string> n_names = {};
vector<string> n_prefix = {};
vector<string> n_title = {};
int numName = 0;
int numPrefixes = 0;
int numTitles = 0;

vector<Character> currentPlayers = {};
