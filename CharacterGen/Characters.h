#ifndef CHARACTERS_H
#define CHARACTERS_H

class Character {
private:

	class Actions {
	public:
		char* name = "Base";
		int classID = 0;

		/* Declare Base Actions */
		virtual void attack(Character &target) = 0;
		virtual void defense(Character &target);
		virtual void buff(Character &target);
		virtual void debuff(Character &target);

		/* TODO: Prefixes & Suffixes for actions */
	};

	class Fire : public Actions {

	public:
		Fire();

		/* class overrides */
		void attack(Character &target);
		void defense(Character &target);
		void buff(Character &target);
		void debuff(Character &target);
	};

	class Water : public Actions {

	public:
		Water();

		/* class overrides */
		void attack(Character &target);
		void defense(Character &target);
		void buff(Character &target);
		void debuff(Character &target);
	};

	class Earth : public Actions {

	public:
		Earth();

		/* class overrides */
		void attack(Character &target);
		void defense(Character &target);
		void buff(Character &target);
		void debuff(Character &target);
	};

	class Air : public Actions {

	public:
		Air();

		/* class overrides */
		void attack(Character &target);
		void defense(Character &target);
		void buff(Character &target);
		void debuff(Character &target);
	};

	/* Base Stats */
	float health = 40;
	float damage = 1.5f;
	float defense = 1.5f;

	/* Visual Markers */
	int head; //(0 - 6)
	int body; //(0 - 11)
	int legs; //(0 - 8)
	int base; //base visual apprearance (0 - 4)

	/* Progression Values */
	float experience;
	float level;

	/* Program Specific Values */
	unsigned long long ID;
	int cID = 0;

	/* Private Methods */

	/* Creates the Player Title */
	string GenNames(string first);

	/* Outputs Character Info */
	void output();

	/* Generation Method which uses User ID to seed new creation */
	void generate(string name);

public:
	string name;
	Actions *action;

	Character(string pName);
	~Character();

	int classID();
};

#endif // !CHARACTERS_H
