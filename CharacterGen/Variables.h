#pragma once
#ifndef VARIABLES_H

#define VARIABLES_H

/* program internal values */
extern bool running;
extern bool debug;
extern int in_id;

/* database lookup values */
extern bool db_searching; //to initiate loop as db is queried
extern bool db_idfound; //subloop to find userID
extern char* db_rName; //return name if ID is found
extern int db_rAge; //return age if ID is found (level)

extern vector<string> n_names;
extern vector<string> n_prefix;
extern vector<string> n_title;
extern int numName;
extern int numPrefixes;
extern int numTitles;

extern vector<Character> currentPlayers;

#endif // !VARIABLES_H
