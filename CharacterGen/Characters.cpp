#include "stdafx.h"

void Character::Actions::attack(Character &target) {
	Log::Msg("Base Attack");
	if (target.cID == this->classID)
		Log::Msg("Attack Class ==");
	else
		Log::Msg("Attack Class !=");
}
void Character::Actions::defense(Character &target) {
	Log::Msg("Base Defense");
}
void Character::Actions::buff(Character &target) {
	Log::Msg("Base Buff");
}
void Character::Actions::debuff(Character &target) {
	Log::Msg("Base Debuff");
}

Character::Fire::Fire() {
	this->name = "Fire";
	this->classID = 1;
}
/* class overrides */
void Character::Fire::attack(Character &target) {
	Log::Msg("Fire Attack");
	if (target.cID == this->classID)
		Log::Msg("Attack Class ==");
	else
		Log::Msg("Attack Class !=");
}
void Character::Fire::defense(Character &target) {
	Log::Msg("Fire Defense");
}
void Character::Fire::buff(Character &target) {
	Log::Msg("Fire Buff");
}
void Character::Fire::debuff(Character &target) {
	Log::Msg("Fire Debuff");
}

Character::Water::Water() {
	this->name = "Water";
	this->classID = 2;
}
/* class overrides */
void Character::Water::attack(Character &target) {
	Log::Msg("Water Attack");
	if (target.cID == this->classID)
		Log::Msg("Attack Class ==");
	else
		Log::Msg("Attack Class !=");
}
void Character::Water::defense(Character &target) {
	Log::Msg("Water Defense");
}
void Character::Water::buff(Character &target) {
	Log::Msg("Water Buff");
}
void Character::Water::debuff(Character &target) {
	Log::Msg("Water Debuff");
}

Character::Earth::Earth() {
	this->name = "Earth";
	this->classID = 3;
}
/* class overrides */
void Character::Earth::attack(Character &target) {
	Log::Msg("Earth Attack");
	if (target.cID == this->classID)
		Log::Msg("Attack Class ==");
	else
		Log::Msg("Attack Class !=");
}
void Character::Earth::defense(Character &target) {
	Log::Msg("Earth Defense");
}
void Character::Earth::buff(Character &target) {
	Log::Msg("Earth Buff");
}
void Character::Earth::debuff(Character &target) {
	Log::Msg("Earth Debuff");
}

Character::Air::Air() {
	this->name = "Air";
	this->classID = 4;
}
/* class overrides */
void Character::Air::attack(Character &target) {
	Log::Msg("Air Attack");
	if (target.cID == this->classID)
		Log::Msg("Attack Class ==");
	else
		Log::Msg("Attack Class !=");
}
void Character::Air::defense(Character &target) {
	Log::Msg("Air Defense");
}
void Character::Air::buff(Character &target) {
	Log::Msg("Air Buff");
}
void Character::Air::debuff(Character &target) {
	Log::Msg("Air Debuff");
}


/* Creates the Player Title */
std::string Character::GenNames(std::string first) {
	return first + (string)", the " + n_prefix[(rand() ^ 2) % numPrefixes] + " " + n_title[(rand() ^ 2) % numTitles];
}

/* Outputs Character Info */
void Character::output() {
	try {
		cout << &this->name[0u] << ": " << this->action->name << endl << flush;
		cout << "Health: " << this->health << endl << flush;
		cout << "Damage: " << this->damage << endl << flush;
		cout << "Defense: " << this->defense << endl << flush;
		cout << "Base: " << this->base << endl << flush;

		cout << "Head: " << this->head << endl << flush;
		cout << "Body: " << this->body << endl << flush;
		cout << "Legs: " << this->legs << endl << flush;
	}
	catch (std::exception& e) {
		Log::Error(e.what());
	}
}

/* Generation Method which uses User ID to seed new creation */
void Character::generate(string name) {

	Ini file(name, "opal\\characters\\");

	//take input as seed rand;
	DWORD seed = 0ul;

	cout << "Seed Value: " << flush;
	cin >> seed;
	srand(seed ^ 2 * 666);

	this->base = rand() % 4;
	this->head = rand() % 6;
	this->body = rand() % 11;
	this->legs = rand() % 8;
	this->name = GenNames(name);
	this->cID = rand() % 5;

	switch (this->cID) {
	default: action = new Fire; Log::Msg("Fire Class Selected"); break;
	case 1: action = new Fire; Log::Msg("Fire Class Selected"); break;
	case 2: action = new Earth; Log::Msg("Earth Class Selected"); break;
	case 3: action = new Water; Log::Msg("Water Class Selected"); break;
	case 4: action = new Air; Log::Msg("Air Class Selected"); break;
	}

	file.write("Core", "Name", this->name);
	file.write_f(this->health, "Core", "Health");
	file.write_f(this->damage, "Core", "Damage");
	file.write_f(this->defense, "Core", "Defense");
	file.write_i(this->cID, "Core", "Class");

	file.write_i(this->base, "Visual", "Base");
	file.write_i(this->head, "Visual", "Head");
	file.write_i(this->body, "Visual", "Body");
	file.write_i(this->legs, "Visual", "Legs");

	file.write_f(this->experience, "Progression", "XP");
	file.write_f(this->level, "Progression", "Level");

	this->output();
}

Character::Character(string pName) {
	//this->name = "invalid"; //replace with db lookup or equivalent

	Ini file(pName, "opal\\characters\\");
	try
	{
		if (file.keyExist("Core", "Name")) {
			Log::Msg("File Exists");
			this->name = file.read("Core", "Name");
			this->health = file.read_f("Core", "Health");
			this->damage = file.read_f("Core", "Damage");
			this->defense = file.read_f("Core", "Defense");
			this->cID = file.read_i("Core", "Class");

			this->base = file.read_i("Visual", "Base");
			this->head = file.read_i("Visual", "Head");
			this->body = file.read_i("Visual", "Body");
			this->legs = file.read_i("Visual", "Legs");

			switch (cID) {
			default: action = new Fire; Log::Msg("Fire Class Selected"); break;
			case 1: action = new Fire; Log::Msg("Fire Class Selected"); break;
			case 2: action = new Earth; Log::Msg("Earth Class Selected"); break;
			case 3: action = new Water; Log::Msg("Water Class Selected"); break;
			case 4: action = new Air; Log::Msg("Air Class Selected"); break;
			}
			this->output();
		}
		else {
			Log::Msg("File Doesn't Exist");
			this->generate(pName);
		}
	}
	catch (const std::exception& e)
	{
		Log::Error(e.what());
	}
}
Character::~Character() {}

int Character::classID()
{
	return this->cID;
}
