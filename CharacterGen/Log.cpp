#include "stdafx.h"

void Log::Msg(std::string msg) {
		if (!debug)
			return;
		cout << "DBG MSG: " << msg << endl << flush;
	}
void Log::Error(std::string msg) {
		if (!debug)
			return;
		cout << "DBG ERROR: " << msg << endl << flush;
	}
void Log::Debug(std::string msg) {
		if (!debug)
			return;
		cout << msg << endl << flush;
	}
