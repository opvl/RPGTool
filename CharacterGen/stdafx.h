// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <random>
#include <fstream>
#include <sstream>
#include <time.h>
#include <Windows.h>
#include <windows.h>
#include <shlobj.h>

using namespace std;

// TODO: reference additional headers your program requires here
#include "Characters.h"
#include "ini.h"
#include "Actions.h"
#include "Log.h"
#include "Variables.h"